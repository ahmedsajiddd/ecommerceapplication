'use strict'
const bodyParser = require('body-parser')
let csrf_validation_flag = false
let auth_token_flag = false
let load_routes = false
const users = require('../Controller/users.js')
const category = require('../Controller/category.js')
const product = require('../Controller/product.js')

/*ZGV2ZWxvcGVyU21hcnQ=*/
module.exports = routes

function routes(app) {
    app.use(bodyParser.json())
    app.use(function(req, res, next) {
        logger.info(" Cors ")
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
        res.header('Access-Control-Allow-Headers', 'Accept,Content-Type,Authorization,Cookie')
        res.header('Access-Control-Allow-Credentials', 'true')
        next()
    })
    app.use(function(req, res, next) {
        logger.info(req['originalUrl'])
        csrf_validation_flag = require('../config/csrf_auth.js')(req.headers)
        logger.info(csrf_validation_flag)
        if (csrf_validation_flag == true) {
            global.validate_api = true
            app.use('/users', users)
            app.use('/category', category)
            app.use('/product', product)
        } else {
            res.status(401).json({
                status: 'Unauthorized',
            })
        }
        next()
    })
}