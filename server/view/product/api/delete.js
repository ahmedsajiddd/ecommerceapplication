'use strict'

const sendResponse = require("../../../helpers/sendResponse")

module.exports = deleteCategory

function deleteCategory(req, res, sendResponse ) {
    var today = new Date();
    let data = {};
    let message = "";
    let success = "";
    let sql = "";
    let id = req.body.id
    if (id == "" || id == undefined || id == 0) {
        let response = {
            type: "bad_request",
            id: id
        }
        return sendResponse(req, res, response);
    }
    if (id != "all" || id != "" || id != undefined) {
       sql = "DELETE FROM product WHERE product_id = '" + id + "'";
    }
    console.info("sql : "+ sql)
    con.query(sql, function(err, result) {
        if (err) {
            let respose = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, respose);
        } else {
            success = true;
            data = result;
            message = "Product Deleted Succesfully"
            const respose = {
                type: "response",
                success: success,
                data: data,
                message: message
            }
            sendResponse(req, res, respose);
        }
    })
}