const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let sendResponse = require('../helpers/sendResponse');
router
    .post(['/login'], (req, res) => {
        require('../view/user/api/loginValidate.js')(req, res, sendResponse);
    })
module.exports = router;