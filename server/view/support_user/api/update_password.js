'use strict';
var sha1 = require('sha1');
module.exports = update;
function update(req, res, sendResponse) {
    var today = new Date();
    let data = {};
    let message = "";
    let success = "";
    var validate_sql = "";
    let user_role  =   req.query.user_role;
    if(user_role == "" || user_role == undefined){
        let response = {
            type         : "bad_request",
            user_role    : user_role
        }
      return  sendResponse(req,res,response);
    }
    if(user_role !="consumer"){
        validate_sql = "SELECT * FROM users WHERE user_id='"+req.query.user_id+"' AND utility_board_id='"+req.query.utility_board_id+"' AND sub_division_id='"+req.query.sub_division_id+"' AND agency_id='"+req.query.agency_id+"'";
    }else{
        validate_sql = "SELECT * FROM users as U LEFT JOIN consumer_accounts as CA ON CA.user_id=U.user_id WHERE CA.consumer_account_number='"+req.query.consumer_account_number+"' AND utility_board_id='"+req.query.utility_board_id+"' AND sub_division_id='"+req.query.sub_division_id+"' AND agency_id='"+req.query.agency_id+"'";
    }
    con.query(validate_sql, function (err, validate) {
        if (err) {
            let respose = {
                type    : "db_error",
                error   : err
            }
            sendResponse(req, res, respose);
        } else {
            if (validate.length !=1) {
                let response = {
                    type    : "bad_request",
                    response: "bad_request",
                    message : "bad_request"
                }
                sendResponse(req, res, response);
            } else {
                let insert_data = {
                    "password"          : sha1(req.body.password),
                    "updated_by"        : req.body.updated_by,
                    "updated_on"        : today
                }
                var sql = "";
                if(user_role !="consumer"){
                     sql = "UPDATE users  SET ? WHERE user_id='"+box.query.user_id+"' AND utility_board_id = '"+req.query.utility_board_id+"' AND sub_division_id='"+req.query.sub_division_id+"' AND agency_id='"+req.query.agency_id+"'";
                }else{
                     sql = "UPDATE users U, consumer_accounts CA  SET ? WHERE CA.user_id= U.user_id AND CA.consumer_account_number='"+box.query.consumer_account_number+"' AND CA.utility_board_id = '"+req.query.utility_board_id+"' AND CA.sub_division_id='"+req.query.sub_division_id+"' AND CA.agency_id='"+req.query.agency_id+"'";
                }
                con.query(sql, insert_data, function (error, result) {
                    if (error) {
                        let respose = {
                            type    : "db_error",
                            error   : error
                        }
                        sendResponse(req, res, respose);
                    } else {
                        const respose = {
                            success : true,
                            type    : 'response',
                            data    : result,
                            message : "Password Updated successfully!"
                        }
                        sendResponse(req, res, respose);
                    }
                });
            }
        }
    }); 
}