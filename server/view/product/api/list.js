'use strict'
const con = require("../../../config/database");

module.exports = list;

function list(req, res, sendResponse) {
    let sql = "";
    let data = {};
    let message = "";
    let success = "";
    let id = req.query.id
    console.info(req.query)

    if (id == "all") {
        sql = "select * from product where product_id is not null;"
    } else {
        sql = "select * from product where product_id = '" + id + "'"
    }
    console.info("sql : " + sql)
    con.query(sql, function(err, result) {
        console.info(err)
        if (err) {
            let respose = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, respose);
        } else {
            if (result.length >= 1) {
                success = true;
                data = result;
                message = "Got data"
            } else {
                success = false;
                data = {};
                message = "No Records Found !";
            }
            const respose = {
                type: "response",
                success: success,
                data: data,
                message: message
            }
            sendResponse(req, res, respose);
        }
    })
}