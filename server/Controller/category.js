const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let sendResponse = require('../helpers/sendResponse');

router
    .get(['/list'], (req, res) => {
        require('../view/category/api/list.js')(req, res, sendResponse);
    })
    .post(['/create'], (req, res) => {
        require('../view/category/api/create.js')(req, res, sendResponse);
    })
    .put(['/update'], (req, res) => {
        require('../view/category/api/update.js')(req, res, sendResponse);
    })
    .delete(['/delete'], (req, res) => {
        require('../view/category/api/delete.js')(req, res, sendResponse);
    })
module.exports = router