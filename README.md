# ecommerceApplication

## Postman Api Playground link 

"https://www.getpostman.com/collections/9bc73efc21b904587dbd"

### Instructions to use the Api's

1. Clone the repository using the following command 
``` git clone https://gitlab.com/ahmedsajiddd/ecommerceapplication.git ```
2. Create the database named ecommerce and import the sql dump provided and change the credentials of     according in dbconfig file placed under server/helpers/dbconfig.json
3. Install the dependencies and run the application using following comands
``` npm i && node server.js  ```
4. Get jwt token using login api by using the credentials provided below
      "email_id" : "admin@ecommerce.com", "password" : "test" 
5. Import all the api routes to post using the Postman Api Link 
``` https://www.getpostman.com/collections/9bc73efc21b904587dbd ```

### Contact for further info
email-id : ahmedsajiddd@gmail.com , Contact Number - 7899011025
