'use strict'

const con = require("../../../config/database");

module.exports = search

async function search(req, res, sendResponse) {
    var today = new Date();
    let data = {};
    let message = "";
    let success = "";
    let searchType = req.query.searchType
    let productName = req.query.productName
    let categoryName = req.query.categoryName
    let id = req.query.id;
    let whereCondition = ""
    let joinCondition = " left join category c on c.category_id = p.category_id "
    let responseData = {};
    if (searchType == "getAll") {
        whereCondition = ""
    } else if (searchType == "getOne") {
        whereCondition = "where product_id = '" + id + "'";
    } else if (searchType == "autoComplete") {
        whereCondition = "where name like '%" + productName + "%'"
    } else if (searchType == "categoryName") {
        whereCondition = "where c.name like '%" + categoryName + "%'"
    }
    let sql = "";
    if (id == "all" && searchType == "getAll") {
        sql = "select * from product";
    } else if (id != "all" && searchType == "getOne") {
        sql = "select * from product " + whereCondition;
    } else if (id == "all" && searchType == "autoComplete") {
        sql = "select * from product ";
    } else if (id == undefined && searchType == "category") {
        sql = 'select c.name as category, p.category_id, p.name as product_name, p.price, p.description from product p left join category c on c.category_id = p.category_id ' + whereCondition
    }
    con.query(sql, (err, result) => {
        if (err) {
            let respose = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, respose);
        } else {
            if (result.length >= 1) {
                responseData.allData = result;
                let firstQuery = "select count(p.product_id) as total_records from product as p ";
                let secondSql = "";
                if (searchType == "getAll") {
                    secondSql = firstQuery + whereCondition;
                } else if (searchType == "getOne") {
                    secondSql = firstQuery + whereCondition;
                } else if (searchType == "category") {
                    secondSql = firstQuery + joinCondition + whereCondition;
                } else if (searchType == "autoComplete") {
                    secondSql = firstQuery + whereCondition;
                }
                con.query(secondSql, (error, resul) => {
                    if (error) {
                        console.info(error);
                    } else {
                        responseData.totalCountData = resul;
                        success = true;
                        data = responseData;
                        message = "Got data"
                    }
                    const respose = {
                        type: "response",
                        success: success,
                        data: data,
                        message: message
                    }
                    sendResponse(req, res, respose);
                });
            } else {
                success = false;
                data = {};
                message = "No Records Found !";
                const respose = {
                    type: "response",
                    success: success,
                    data: data,
                    message: message
                }
                sendResponse(req, res, respose);
            }
        }
    });
}