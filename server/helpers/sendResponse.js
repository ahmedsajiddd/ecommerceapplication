module.exports = sendResponse;

function sendResponse(req, res, obj) {
    let message = "";
    let status = "";
    if (obj.type == 'db_error') {
        return res.status(400).json({
            status: false,
            message: "Invalid Data Processed",
            error_message: obj.error.sqlMessage
        });
    } else if (obj.type == 'response') {
        if (obj.message && obj.message != undefined) {
            message = obj.message;
            status = obj.success;
        }
        if (req.headers['x-access-token']) {
            token = req.headers['x-access-token'];
        } else {
            token = obj.token
            status = obj.success;
        }
        let response = {
            status: status,
            message: message,
            data: obj.data,
            token: token,
        }
        return res.status(200).json(response);
    } else if (obj.type == 'bad_request') {
        return res.status(400).json({
            status: false,
            message: "Bad Request"
        });
    } else if (obj.type == 'request_for_csrf') {
        return res.status(401).json({
            status: 'Unauthorized'
        });
    } else {
        return res.status(401).json({
            status: 'Unauthorized',
        });
    }
}