const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const config = require('../helpers/jwt.json');
const Joi = require('joi');
let schema = require('../helpers/schema.js')
let createSchema = require('../view/product/schema/createSchema.js')
let sendResponse = require('../helpers/sendResponse');

router
    .get(['/list'], (req, res) => {
        require('../view/product/api/list.js')(req, res, sendResponse);
    })
    .post(['/create'], (req, res) => {
        require('../view/product/api/create.js')(req, res, sendResponse)
    })
    .put(['/update'], (req, res) => {
        require('../view/product/api/update.js')(req, res, sendResponse);
    })
    .delete(['/delete'], (req, res) => {
        require('../view/product/api/delete.js')(req, res, sendResponse);
    })
    .get(['/search'], (req, res) => {
        require('../view/product/api/search.js')(req, res, sendResponse)
    })
module.exports = router