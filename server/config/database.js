var mysql = require('mysql');
var db_config = require('../helpers/dbconfig.json');
const logger = require('./logging')
let db_name = "";
let db_hostname = "";
let db_password = "";
let db_username = "";

if (global.Env == 'dev') {
    db_name = db_config.dev.db_name;
    db_hostname = db_config.dev.db_hostname;
    db_password = db_config.dev.db_password;
    db_username = db_config.dev.db_username;
} else if (global.Env == 'local') {
    db_name = db_config.local.db_name;
    db_hostname = db_config.local.db_hostname;
    db_password = db_config.local.db_password;
    db_username = db_config.local.db_username;
}
var con = mysql.createPool({
    connectionLimit: 10,
    host: db_hostname,
    user: db_username,
    password: db_password,
    database: db_name
});
global.con = con;
con.getConnection(function(err) {
    if (err) {
        logger.error("Error in connecting with database");
        logger.error(err);
        throw err;
    } else {
        logger.info("Database Connected!");
    }
});

module.exports = con;