'use strict'
const con = require("../../../config/database");

module.exports = list;

function list(req, res, sendResponse) {
    let sql = "";
    let data = {};
    let message = "";
    let success = "";
    let id = req.query.id
    let treeviews = req.query.treeviews
    console.info(req.query)

    if (id == "all") {
        sql = "SELECT * FROM category C ORDER BY C.category_id"
    } else if (treeviews == "enable") {
        console.info("entered tree view")
        sql = "select t1.name as lev1, t2.name as lev2, t3.name as lev3 from category as t1"
        sql += " left join category as t2 on t2.parent = t1.category_id left join category as t3 on t3.parent = t2.category_id where t1.name = 'ELECTRONICS'"  
    }
    console.info("sql : " + sql)
    con.query(sql, function(err, result) {
        console.info(err)
        if (err) {
            let respose = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, respose);
        } else {
            if (result.length >= 1) {
                success = true;
                data = result;
                message = "Got data"
            } else {
                success = false;
                data = {};
                message = "No Records Found !";
            }
            const respose = {
                type: "response",
                success: success,
                data: data,
                message: message
            }
            sendResponse(req, res, respose);
        }
    })
}