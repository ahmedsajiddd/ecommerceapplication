'use strict'

const con = require("../../../config/database");

module.exports = create;

function create(req, res, sendResponse) {
    var today = new Date();
    let data = {};
    let message = "";
    let success = "";
    let parent = req.body.parent
   console.info(req.body) 
    let insertData = {
        "name": req.body.name,
        "parent": parent
    }
    if (parent == "" || parent == undefined) {
        let response = {
            type: "bad request",
            parent: parent
        }
        return sendResponse(req, res, response);
    } 
    let validateSql = "SELECT * FROM category WHERE name = '" +  req.body.name +"' ";
    con.query(validateSql, function(err, response) {
        if (err) {
            let respose = {
                type: "db_error",
                error: err
            }
            sendResponse(req, res, respose);
        } else {
            if (response.length > 0) {
                const respose = {
                    success: false,
                    type: 'response',
                    message: "Category Name Already Exist"
                }
                sendResponse(req, res, respose);
            } else {
                console.info("enteerd category create")
                let sql = "INSERT INTO category SET ?"
                console.info(sql)
                con.query(sql, insertData, function(error, result) {
                    if (error) {
                        let respose = {
                            type: "db_error",
                            error: error
                        }
                        sendResponse(req, res, respose);
                    } else {
                        const respose = {
                            success: true,
                            type: 'response',
                            message: "New Category Created successfully!"
                        }
                        sendResponse(req, res, respose);
                    }
                })
            }
        }
    })
}